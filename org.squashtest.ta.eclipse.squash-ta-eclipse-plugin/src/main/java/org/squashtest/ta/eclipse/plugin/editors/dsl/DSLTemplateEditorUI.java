/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;


import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.ui.editors.text.templates.ContributionContextTypeRegistry;
import org.eclipse.ui.editors.text.templates.ContributionTemplateStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.service.prefs.BackingStoreException;
import org.squashtest.ta.eclipse.plugin.Activator;
import org.squashtest.ta.eclipse.plugin.editors.completion.DSLCompletionPossibleType;
import org.squashtest.ta.eclipse.plugin.editors.completion.DSLTemplateContextType;

/**
 * The main plugin class to be used in the desktop.
 */
public class DSLTemplateEditorUI  {
	/** Key to store custom templates. */
	private static final String CUSTOM_TEMPLATES_KEY= "org.squashtest.ta.eclipse.plugin.editors.customtemplates"; //$NON-NLS-1$

	/** The shared instance. */
	private static DSLTemplateEditorUI fInstance;

	/** The template store. */
	private TemplateStore fStore;
	
	/** The context type registry. */
	private ContributionContextTypeRegistry fRegistry;

	private DSLTemplateEditorUI() {
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return the shared instance
	 */
	public static DSLTemplateEditorUI getDefault() {
		if (fInstance == null)
			fInstance= new DSLTemplateEditorUI();
		return fInstance;
	}

	/**
	 * Returns this plug-in's template store.
	 *
	 * @return the template store of this plug-in instance
	 */
	public TemplateStore getTemplateStore() {
		if (fStore == null) {
			fStore= new ContributionTemplateStore(getContextTypeRegistry(), Activator.getDefault().getPreferenceStore(), CUSTOM_TEMPLATES_KEY);
			try {
				fStore.load();
			} catch (IOException e) {
				Activator.getDefault().getLog().log(new Status(IStatus.ERROR, "org.eclipse.ui.examples.javaeditor", IStatus.OK, "", e)); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return fStore;
	}

	/**
	 * Returns this plug-in's context type registry.
	 *
	 * @return the context type registry for this plug-in instance
	 */
	public ContextTypeRegistry getContextTypeRegistry() {
		if (fRegistry == null) {
			// create an configure the contexts available in the template editor
			fRegistry= new ContributionContextTypeRegistry();
			//fRegistry.addContextType(DSLTemplateContextType.TA_CONTEXT_TYPE);
			fRegistry.addContextType(DSLCompletionPossibleType.PHASE.getId());
			fRegistry.addContextType(DSLCompletionPossibleType.HEAD_KEY_WORD.getId());
			fRegistry.addContextType(DSLCompletionPossibleType.KEY_WORD.getId());
		}
		return fRegistry;
	}

	/* Forward plug-in methods to javaeditor example plugin default instance */
	public ImageRegistry getImageRegistry() {
		return Activator.getDefault().getImageRegistry();
	}

	public static ImageDescriptor imageDescriptorFromPlugin(String string, String default_image) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(string, default_image);
	}

	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	public void savePluginPreferences() {
		try {
			Activator.INSTANCE.getNode(Activator.PLUGIN_ID).flush();
		} catch (BackingStoreException e) {
			Activator.log(e);
		}
	}

}
