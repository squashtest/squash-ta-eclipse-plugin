/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * Base class for Squash TA target definition file creation wizards.
 * @author edegenetais
 *
 */
public abstract class AbstractTargetWizard extends Wizard implements INewWizard{

	private TargetDefinitionWizardPage targetDefinition;

	private ISelection selection;
	
	public AbstractTargetWizard() {
		setNeedsProgressMonitor(true);
	}

	/**
	 * File contents will be created from user input.
	 * @throws IOException 
	 */
	protected InputStream createContentStream(Properties dbProperties) throws IOException {
		ByteArrayOutputStream os=new ByteArrayOutputStream();
		
		OutputStreamWriter writer;
		
		try {
			writer = new OutputStreamWriter(os, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			writer = new OutputStreamWriter(os);
		}
		
		String shebangMark;
		if(getShebangMark()==null){
			shebangMark=null;
		}else{
			shebangMark="!"+getShebangMark();
		}
		dbProperties.store(writer, shebangMark);
		
		return new ByteArrayInputStream(os.toByteArray());
	}

	/**
	 * Method overriden by subclasses to provide the target type-specific
	 * shebang mark to include in the target definition properties file.
	 * 
	 * @return the mark value (without the #! prefix)
	 */
	protected abstract String getShebangMark();
	
	protected boolean displayExceptionAndFail(InvocationTargetException e) {
		MessageDialog.openError(getShell(), "Error", e.getTargetException().getMessage());
		return false;
	}
	
	void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "squash-ta-eclipse-plugin", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */
	protected void doFinish(String containerName, String fileName, Properties dbProperties,
			IProgressMonitor monitor) throws CoreException {
				// create a sample file
				monitor.beginTask("Creating " + fileName, 2);
				IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				IResource resource = root.findMember(new Path(containerName));
				if (!resource.exists() || !(resource instanceof IContainer)) {
					throwCoreException("Container \"" + containerName + "\" does not exist.");
				}
				IContainer container = (IContainer) resource;
				final IFile file = container.getFile(new Path(fileName));
				try {
					InputStream stream = createContentStream(dbProperties);
					if (file.exists()) {
						file.setContents(stream, true, true, monitor);
					} else {
						file.create(stream, true, monitor);
					}
					stream.close();
				} catch (IOException e) {
				}
				monitor.worked(1);
				monitor.setTaskName("Opening file for editing...");
				getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						IWorkbenchPage page =
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						try {
							IDE.openEditor(page, file, true);
						} catch (PartInitException e) {
						}
					}
				});
				monitor.worked(1);
	}
	
	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String containerName = targetDefinition.getContainerName();
		final String fileName = "src/squashTA/targets/"+targetDefinition.getTargetName()+".properties";
		
		final Properties dbProperties = extractTargetProperties();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(containerName, fileName, dbProperties,monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			return displayExceptionAndFail(e);
		}
		return true;
	}

	/**
	 * Extension point so that concrete subclasses give the list of all defined
	 * target properties.
	 * 
	 * @return the property values given to the wizard.
	 */
	protected abstract Properties extractTargetProperties();

	/**
	 * Adding the page to the wizard.
	 */
	public void addPages() {
			targetDefinition = new TargetDefinitionWizardPage(selection);
			addPage(targetDefinition);
			addSpecificPages();
	}
	/**
	 * Method to add pages specific to a subclass of this abstract wizard after the initial target definition page.
	 */
	protected abstract void addSpecificPages();

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}