/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.templates.TemplateProposal;
import org.squashtest.ta.eclipse.plugin.editors.analyser.LineAnalyseResult;
import org.squashtest.ta.eclipse.plugin.model.DSLClause;
import org.squashtest.ta.eclipse.plugin.model.DSLHeadClause;
import org.squashtest.ta.eclipse.plugin.model.DSLPhase;

public class ComputeInstructionProposal {

	private static final class ProposalComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			return ((TemplateProposal) o2).getRelevance() - ((TemplateProposal) o1).getRelevance();
		}
	}

	private static final Comparator fgProposalComparator = new ProposalComparator();

	private LineAnalyseResult result;

	public ComputeInstructionProposal(LineAnalyseResult result) {
		this.result = result;
	}

	public List<ICompletionProposal> getProposal(DSLCompletionPossibleType dslCompletionPossibleType) {
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		String searched = result.getSearched();
		switch (dslCompletionPossibleType) {
		case PHASE:
			List<DSLPhase> possiblePhase = DSLPhase.startWith(searched);
			IDSLProposalFact<DSLPhase> phaseFactory = new PhaseProposalFactory();
			proposals.addAll(phaseFactory.getProposal(possiblePhase, result));
			break;
		case HEAD_KEY_WORD:
			List<DSLHeadClause> possibleHeadClause = DSLHeadClause.startWith(searched);
			IDSLProposalFact<DSLHeadClause> headClauseFactory = new HeadClauseProposalFactory();
			proposals.addAll(headClauseFactory.getProposal(possibleHeadClause, result));
			break;
		case KEY_WORD:
			IDSLProposalFact<DSLClause> clauseFactory = new ClauseProposalFactory(result.getHeadClause());
			proposals.addAll(clauseFactory.getProposal(result.getPossibleClauseList(), result));
			break;
		default:
			// Not a member of the enum => return an empty list
			break;
		}
		return proposals;
	}

	public List<ICompletionProposal> getTemplateProposal(DSLCompletionPossibleType dslCompletionPossibleType,
			Map<String, List<ICompletionProposal>> proposalMap) {
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		switch (dslCompletionPossibleType) {
		case PHASE:
			// No template for setup phase
			break;

		case HEAD_KEY_WORD:
			retrieveHeadKeyWordProposal(proposals, proposalMap);
			break;

		case KEY_WORD:
			retrieveKeyWordProposal(proposals, proposalMap);
			break;

		default:
			// Not a member of the enum => return an empty list
			break;
		}

		return proposals;

	}
	
	private void retrieveHeadKeyWordProposal(List<ICompletionProposal> proposals, Map<String, List<ICompletionProposal>> proposalMap) {
		List<ICompletionProposal> templateProposalList = new ArrayList<ICompletionProposal>();
		for (Collection<ICompletionProposal> completionProposal : proposalMap.values()) {
			templateProposalList.addAll(completionProposal);
		}
		Collections.sort(templateProposalList, fgProposalComparator);
		proposals.addAll(templateProposalList);
	}
	
	private void retrieveKeyWordProposal(List<ICompletionProposal> proposals, Map<String, List<ICompletionProposal>> proposalMap){
		List<ICompletionProposal> templateProposalList = new ArrayList<ICompletionProposal>();
		templateProposalList = new ArrayList<ICompletionProposal>();
		Set<String> patternPieceList = proposalMap.keySet();
		List<DSLClause> poss = result.getPossibleClauseList();
		for (String patternPiece : patternPieceList) {
			DSLClause clause = DSLClause.get(patternPiece);
			if (clause != null && poss.contains(clause)) {
				templateProposalList.addAll(proposalMap.get(patternPiece));
			}
		}
		Collections.sort(templateProposalList, fgProposalComparator);
		proposals.addAll(templateProposalList);
	}

}
