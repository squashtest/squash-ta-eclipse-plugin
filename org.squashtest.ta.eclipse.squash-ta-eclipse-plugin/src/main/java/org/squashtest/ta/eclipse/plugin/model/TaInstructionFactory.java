/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.model;

import java.util.ArrayList;
import java.util.List;

public final class TaInstructionFactory {

	private static DSLInstruction define;

	private static DSLInstruction load;

	private static DSLInstruction convert;

	private static DSLInstruction execute;

	private static DSLInstruction assertInst;

	private static DSLInstruction verify;

	private TaInstructionFactory() {

	}

	public static DSLInstruction getInstruction(DSLHeadClause headClause) {
		DSLInstruction instruction;
		switch (headClause) {
		case DEFINE:
			instruction = createDefine();
			break;
		case LOAD:
			instruction = createLoad();
			break;
		case CONVERT:
			instruction = createConvert();
			break;
		case EXECUTE:
			instruction = createExecute();
			break;
		case ASSERT:
			instruction = createAssert();
			break;
		case VERIFY:
			instruction = createVerify();
			break;
		default:
			// else noop
			instruction = null;
			break;
		}
		return instruction;
	}

	private static DSLInstruction createVerify() {
		if (verify == null) {
			List<DSLClause> possibleClause = assertionClause();
			verify = new DSLInstruction(DSLHeadClause.VERIFY, possibleClause);
		}
		return verify;
	}

	private static DSLInstruction createAssert() {
		if (assertInst == null) {
			List<DSLClause> possibleClause = assertionClause();
			assertInst = new DSLInstruction(DSLHeadClause.ASSERT, possibleClause);
		}
		return assertInst;
	}

	private static List<DSLClause> assertionClause() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.IS);
		possibleClause.add(DSLClause.HAS);
		possibleClause.add(DSLClause.DOES);
		possibleClause.add(DSLClause.WITH);
		possibleClause.add(DSLClause.THAN);
		possibleClause.add(DSLClause.THE);
		possibleClause.add(DSLClause.USING);
		return possibleClause;
	}

	private static DSLInstruction createExecute() {
		if (execute == null) {
			List<DSLClause> possibleClause = new ArrayList<DSLClause>();
			possibleClause.add(DSLClause.WITH);
			possibleClause.add(DSLClause.ON);
			possibleClause.add(DSLClause.USING);
			possibleClause.add(DSLClause.AS);
			execute = new DSLInstruction(DSLHeadClause.EXECUTE, possibleClause);
		}
		return execute;
	}

	private static DSLInstruction createConvert() {
		if (convert == null) {
			List<DSLClause> possibleClause = new ArrayList<DSLClause>();
			possibleClause.add(DSLClause.TO);
			possibleClause.add(DSLClause.AS);
			possibleClause.add(DSLClause.USING);
			convert = new DSLInstruction(DSLHeadClause.CONVERT, possibleClause);
		}
		return convert;
	}

	private static DSLInstruction createLoad() {
		if (load == null) {
			List<DSLClause> possibleClause = new ArrayList<DSLClause>();
			possibleClause.add(DSLClause.FROM);
			possibleClause.add(DSLClause.AS);
			load = new DSLInstruction(DSLHeadClause.LOAD, possibleClause);
		}
		return load;
	}

	private static DSLInstruction createDefine() {
		if (define == null) {
			List<DSLClause> possibleClause = new ArrayList<DSLClause>();
			possibleClause.add(DSLClause.AS);
			define = new DSLInstruction(DSLHeadClause.DEFINE, possibleClause);
		}
		return define;
	}

}
