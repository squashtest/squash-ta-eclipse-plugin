/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.squashtest.ta.eclipse.plugin.editors.rules.PhaseRule;

/**
 * This class defines syntactic coloring rules for whole lines.
 * @author edegenetais
 *
 */
enum DSLPartitionRuleSet{
	/** Coloration rule for macro call DSL lines */
	MACRO_CALL("__macro_definition",DSLColorMapping.MACRO_CALL){
		@Override
		public IPredicateRule getRule() {
			return new EndOfLineRule("# ", getToken());
		}
	},
	/** Coloration rule for comment DSL lines */
	DSL_COMMENT("__dsl_comment",DSLColorMapping.DSL_COMMENT){
		@Override
		public IPredicateRule getRule() {
			return new EndOfLineRule("//", getToken());
		}
	},
	/** Coloration rule for phase marker DSL lines */
	PHASE_MARKER("__phase_marker",DSLColorMapping.PHASE_MARKER,SWT.BOLD){
		@Override
		public IPredicateRule getRule() {
			return new PhaseRule(getToken());
		}
	}
	;
	
	private String id;
	
	private DSLColorMapping associatedColor;
	
	// Could be SWT.NORMAL, SWT.BOLD, SWT.ITALIC 
	private int style;
	
	private DSLPartitionRuleSet(String id, DSLColorMapping associatedColor){
		this.id=id;
		this.associatedColor=associatedColor;
		this.style=SWT.NORMAL;
	}
	
	private DSLPartitionRuleSet(String id, DSLColorMapping associatedColor, int style){
		this.id=id;
		this.associatedColor=associatedColor;
		this.style=style;
	}
	
	public static List<String> getContentTypeIdList(){
		List<String>idList=new ArrayList<String>();
		for(DSLPartitionRuleSet type:DSLPartitionRuleSet.values()){
			idList.add(type.getId());
		}
		return idList;
	}
	public static String[] getContentTypeIdTable(){
		return getContentTypeIdList().toArray(new String[values().length]);
	}
	
	public String getId(){
		return id;
	}
	public DSLColorMapping getAssociatedColor(){
		return associatedColor;
	}
	
	public int getStyle() {
		return style;
	}
	
	public IToken getToken(){
		return new Token(id);
	}
	
	public abstract IPredicateRule getRule();
}
