/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class SshTargetPropertiesWizardPage extends Page {

	private Text port;
	private Text username;
	private Text password;
	private Text hostname;
	private Button enablePortField;
	
	public SshTargetPropertiesWizardPage() {
		super(SshTargetPropertiesWizardPage.class.getSimpleName());
		setTitle("Enter ssh server properties");
		setDescription("This form lets you define the ssh server properties.");
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite uiContainer=createPageUIContainer(parent, 3);
		
		addLabel(uiContainer, "Server hostname",2);
		hostname=createInput(uiContainer);
		addPageRule(new MandatoryRule(hostname, "Server hostname", this));
		setControl(uiContainer);
		
		addLabel(uiContainer, "User name",2);
		username=createInput(uiContainer);
		addPageRule(new MandatoryRule(username, "Server hostname", this));
		setControl(uiContainer);
		
		addLabel(uiContainer, "User password",2);
		password=createInput(uiContainer);
		setControl(uiContainer);
		
		enablePortField = createCheck(uiContainer);
		addLabel(uiContainer, "Server port");
		port=createInput(uiContainer);
		final MandatoryRule portMandatoryRule = new MandatoryRule(port, "Server port", this); 
		addPageRule(portMandatoryRule);
		setControl(uiContainer);
		
		enablePortField.setSelection(false);
		port.setEnabled(false);
		
		/* Add the listeners */
		enablePortField.addSelectionListener( new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				port.setEnabled(enablePortField.getSelection());
				if(enablePortField.getSelection())	{
					portMandatoryRule.activate();
				} else {
					portMandatoryRule.deactivate();
				}
			}
			
		});
		

	}

	public boolean isPortFieldEnable() {
		return enablePortField.getSelection();
	}
	
	public String getPort() {
		return port.getText();
	}

	public String getUsername() {
		return username.getText();
	}

	public String getPassword() {
		return password.getText();
	}

	public String getHostname() {
		return hostname.getText();
	}

}
