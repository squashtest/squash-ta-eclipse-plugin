/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;
import org.squashtest.ta.eclipse.plugin.editors.ColorManager;
import org.squashtest.ta.eclipse.plugin.editors.rules.DSLKeyWordDetector;
import org.squashtest.ta.eclipse.plugin.editors.rules.DSLWhitespaceDetector;
import org.squashtest.ta.eclipse.plugin.model.DSLClause;
import org.squashtest.ta.eclipse.plugin.model.DSLHeadClause;

/**
 * This component scans DSL content to find different kinds of DSL syntactic
 * elements to have them painted in their syntactic color.
 * 
 * @author edegenetais
 * 
 */
public class DSLElementScanner extends RuleBasedScanner {
	
	public DSLElementScanner(ColorManager manager) {
		
		IToken inlineResource = new Token( new TextAttribute(manager.getColor(DSLColorMapping.INLINE_RESOURCE)));

		IToken componentTypeToken = new Token(new TextAttribute(manager.getColor(DSLColorMapping.COMPONENT_TYPE),null,SWT.BOLD));
		
		IToken defaultToken = new Token(new TextAttribute(manager.getColor(DSLColorMapping.DEFAULT)));
		
		List<IRule> ruleList = new ArrayList<IRule>();
		//Add rule for inlines
		ruleList.add( new SingleLineRule("$(", ")", inlineResource));
		
		//Add generic whitespace rule (consumes whitespace to allow completion for other rules).
		ruleList.add( new WhitespaceRule(new DSLWhitespaceDetector()));
		
		WordRule wordRule= new WordRule(new DSLKeyWordDetector(), defaultToken, true);
		DSLHeadClause[] headClauseArray = DSLHeadClause.values();
		for (int i = 0; i < headClauseArray.length; i++) {
			wordRule.addWord(headClauseArray[i].toString(), componentTypeToken);
		}
		DSLClause[] clauseArray = DSLClause.values();
		for (int i = 0; i < clauseArray.length; i++) {
			wordRule.addWord(clauseArray[i].toString(), componentTypeToken);
		}
		

		ruleList.add(wordRule);
		
		setRules(ruleList.toArray(new IRule[ruleList.size()]));
	}
}
