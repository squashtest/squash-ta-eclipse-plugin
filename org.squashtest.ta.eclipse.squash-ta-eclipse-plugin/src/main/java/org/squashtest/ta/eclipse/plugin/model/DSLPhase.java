/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.eclipse.plugin.editors.ProposalMessages;

public enum DSLPhase {
	
	
	SETUP(ProposalMessages.PhaseProposalSetupValue),TEST(ProposalMessages.PhaseProposalTestValue),TEARDOWN(ProposalMessages.PhaseProposalTeardownValue);
	
	private String value;

	private DSLPhase(String value) {
		this.value = value;
	}
	
	public static DSLPhase get(String test) {
		try {
			return DSLPhase.valueOf(test);
		} catch (IllegalArgumentException e) {
			return null;
		}   
	}
	
	public static List<DSLPhase> startWith (String start){
		List<DSLPhase> list = new ArrayList<DSLPhase>();
		for (DSLPhase phase : DSLPhase.values()) {
			if(phase.name().startsWith(start.toUpperCase())){
				list.add(phase);
			}
		}
		return list;
	}
	
	public String getValue () {
		return value;
	}
}
