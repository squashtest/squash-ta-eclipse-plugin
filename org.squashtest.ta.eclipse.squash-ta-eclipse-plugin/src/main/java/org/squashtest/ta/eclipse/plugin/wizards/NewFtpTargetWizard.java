/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public final class NewFtpTargetWizard extends AbstractTargetWizard {

	public static final String HOSTNAME_KEY = "squashtest.ta.ftp.host";
	public static final String USERNAME_KEY = "squashtest.ta.ftp.username";
	public static final String PASSWORD_KEY = "squashtest.ta.ftp.password";
	public static final String PORT_KEY = "squashtest.ta.ftp.port";
	public static final String FILETYPE_KEY = "squashtest.ta.ftp.filetype";
	public static final String SYSTEM_KEY = "squashtest.ta.ftp.system";
	private FtpTargetPropertiesWizardPage targetProperties;

	@Override
	protected String getShebangMark() {
		return "ftp";
	}

	@Override
	protected Properties extractTargetProperties() {
		final Properties ftpProperties = new Properties();
		ftpProperties.setProperty(HOSTNAME_KEY, targetProperties.getHostname());
		if (targetProperties.isPortFieldEnable()) {
			ftpProperties.setProperty(PORT_KEY, targetProperties.getPort());
		}
		ftpProperties.setProperty(USERNAME_KEY, targetProperties.getUsername());
		ftpProperties.setProperty(PASSWORD_KEY, targetProperties.getPassword());
		if (targetProperties.isFileTypeFieldEnable()) {
			ftpProperties.setProperty(FILETYPE_KEY,
					targetProperties.getFileType());
		}
		if (targetProperties.isSystemFieldEnable()) {
			ftpProperties.setProperty(SYSTEM_KEY, targetProperties.getSystem());
		}
		return ftpProperties;
	}

	@Override
	protected void addSpecificPages() {
		targetProperties = new FtpTargetPropertiesWizardPage();
		addPage(targetProperties);
	}

}
