/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.squashtest.ta.eclipse.plugin.editors.analyser.LineAnalyseResult;

public abstract class AbstractProposalFactory <T extends Enum<T>> implements IDSLProposalFact<T> {
		
	public List<ICompletionProposal> getProposal(List<T> possibleList, LineAnalyseResult result)
	{
		List<ICompletionProposal> proposalList = new ArrayList<ICompletionProposal>();
		CompletionValue propValue;
		int searchedLength = result.getSearched().length();
		int replacementBegin = result.getInitialOffSet()-searchedLength;
		for (T potential : possibleList) {
			propValue = create(potential);
			ICompletionProposal proposal = new CompletionProposal( replacementBegin, searchedLength, propValue.getValue(), propValue.getDescription(), propValue.getInfo() );
			proposalList.add(proposal);
		}
		return proposalList;
	}
	
	protected abstract CompletionValue create(T potential);
	
	class CompletionValue{
		
		private String value;
		private String description;
		private String info;
		
		public CompletionValue(String value, String description, String info) {
			super();
			this.value = value;
			this.info = info;
			this.description = description;
		}

		public String getValue() {
			return value;
		}

		public String getInfo() {
			return info;
		}
		
		public String getDescription() {
			return description;
		}

	}
	
}
