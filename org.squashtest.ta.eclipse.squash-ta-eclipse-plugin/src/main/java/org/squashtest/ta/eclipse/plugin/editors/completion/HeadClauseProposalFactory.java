/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import org.squashtest.ta.eclipse.plugin.editors.ProposalMessages;
import org.squashtest.ta.eclipse.plugin.model.DSLHeadClause;

public class HeadClauseProposalFactory extends AbstractProposalFactory<DSLHeadClause> {

	@Override
	protected CompletionValue create(DSLHeadClause potential) {
		CompletionValue value;
		switch (potential) {
		case DEFINE:
			value = new CompletionValue(ProposalMessages.HeadProposalDefineValue, ProposalMessages.HeadProposalDefineDesc, ProposalMessages.HeadProposalDefineDoc);
			break;
		case LOAD:
			value = new CompletionValue(ProposalMessages.HeadProposalLoadValue, ProposalMessages.HeadProposalLoadDesc, ProposalMessages.HeadProposalLoadDoc);
			break;
		case CONVERT:
			value = new CompletionValue(ProposalMessages.HeadProposalConverterValue, ProposalMessages.HeadProposalConverterDesc, ProposalMessages.HeadProposalConverterDoc);
			break;
		case EXECUTE:
			value = new CompletionValue(ProposalMessages.HeadProposalExecuteValue,ProposalMessages.HeadProposalExecuteDesc,ProposalMessages.HeadProposalExecuteDoc);
			break;
		case ASSERT:
			value = new CompletionValue(ProposalMessages.HeadProposalAssertValue,ProposalMessages.HeadProposalAssertDesc,ProposalMessages.HeadProposalAssertDoc);
			break;
		case VERIFY:
			value = new CompletionValue(ProposalMessages.HeadProposalVerifyValue,ProposalMessages.HeadProposalVerifyDesc,ProposalMessages.HeadProposalVerifyDoc);
			break;
		default:
			// Not an element of the enum => return null
			value = null;
			break;
		}
		return value;
	}
	
}
