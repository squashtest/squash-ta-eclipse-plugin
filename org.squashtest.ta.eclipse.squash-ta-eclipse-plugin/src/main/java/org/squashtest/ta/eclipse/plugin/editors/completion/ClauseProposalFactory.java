/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import org.squashtest.ta.eclipse.plugin.editors.ProposalMessages;
import org.squashtest.ta.eclipse.plugin.model.DSLClause;
import org.squashtest.ta.eclipse.plugin.model.DSLHeadClause;

public class ClauseProposalFactory extends AbstractProposalFactory<DSLClause> {

	private DSLHeadClause headClause;
	
	public ClauseProposalFactory(DSLHeadClause headClause){
		this.headClause = headClause;
	}
	
	@Override
	protected CompletionValue create(DSLClause potential) {
		CompletionValue value;
		
		// Test
		//value = new CompletionValue(potential.name(), "k - "+potential.name(), "The potential Clause is: "+potential.name());
		StringBuilder builder;
		switch (potential) {
		case AS:
			builder = new StringBuilder(ProposalMessages.ClauseProposalAsDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalAsValue, ProposalMessages.ClauseProposalAsDesc, builder.toString());
			break;
			
		case FROM:
			builder = new StringBuilder(ProposalMessages.ClauseProposalFromDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalFromValue, ProposalMessages.ClauseProposalFromDesc, builder.toString());
			break;
			
		case TO:
			builder = new StringBuilder(ProposalMessages.ClauseProposalToDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalToValue, ProposalMessages.ClauseProposalToDesc, builder.toString());
			break;
			
		case USING:
			builder = new StringBuilder(ProposalMessages.ClauseProposalUsingDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalUsingValue, ProposalMessages.ClauseProposalUsingDesc, builder.toString());
			break;
	
		case WITH:
			builder = new StringBuilder(ProposalMessages.ClauseProposalWithDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalWithValue, ProposalMessages.ClauseProposalWithDesc, builder.toString());
			break;

		case ON:
			builder = new StringBuilder(ProposalMessages.ClauseProposalOnDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalOnValue, ProposalMessages.ClauseProposalOnDesc, builder.toString());
			break;
			
		case IS:
			builder = new StringBuilder(ProposalMessages.ClauseProposalIsDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalIsValue, ProposalMessages.ClauseProposalIsDesc, builder.toString());
			break;
			
		case HAS:
			builder = new StringBuilder(ProposalMessages.ClauseProposalHasDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalHasValue, ProposalMessages.ClauseProposalHasDesc, builder.toString());
			break;
			
		case DOES:
			builder = new StringBuilder(ProposalMessages.ClauseProposalDoesDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalDoesValue, ProposalMessages.ClauseProposalDoesDesc, builder.toString());
			break;	
		
		case THAN:
			builder = new StringBuilder(ProposalMessages.ClauseProposalThanDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalThanValue, ProposalMessages.ClauseProposalThanDesc, builder.toString());
			break;
		
		case THE:
			builder = new StringBuilder(ProposalMessages.ClauseProposalTheDoc);
			getDoc(builder);
			value = new CompletionValue(ProposalMessages.ClauseProposalTheValue, ProposalMessages.ClauseProposalTheDesc, builder.toString());
			break;
			
		default:
			// Not an element of the enum => return null
			value = null;
			break;
		}
		return value;
	}
	
	private void getDoc(StringBuilder builder){
		String desc=null;
		String doc=null;
		switch (headClause) {
		case DEFINE:
			desc = ProposalMessages.HeadProposalDefineDesc;
			doc = ProposalMessages.HeadProposalDefineDoc;
			break;

		case LOAD:
			desc = ProposalMessages.HeadProposalLoadDesc;
			doc = ProposalMessages.HeadProposalLoadDoc;		
			break;
					
		case CONVERT:
			desc = ProposalMessages.HeadProposalConverterDesc;
			doc = ProposalMessages.HeadProposalConverterDoc;
			break;
			
		case EXECUTE:
			desc = ProposalMessages.HeadProposalExecuteDesc;
			doc = ProposalMessages.HeadProposalExecuteDoc;
			break;
		
		case ASSERT:
			desc = ProposalMessages.HeadProposalAssertDesc;
			doc = ProposalMessages.HeadProposalAssertDoc;
			break;
			
		default:
			break;
		}
		builder.append(desc);
		builder.append("\n");
		builder.append(doc);
	}

}
