/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.regex.Pattern;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;

/**
 * Validation rules for the input in the first page of target creation wizards.
 * @author edegenetais
 *
 */
final class TargetDefinitionRules implements ModifyListener {
	private TargetDefinitionWizardPage target;
	static final Pattern VALID_TARGET_NAME=Pattern.compile("[a-zA-Z0-9_.]+");
	
	public TargetDefinitionRules(TargetDefinitionWizardPage target) {
		this.target=target;
	}
	/**
	 * Ensures that both text fields are set.
	 */
	public void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(target.getContainerName()));
		String fileName = target.getTargetName();

		if (target.getContainerName().length() == 0) {
			target.updateStatus(toString(),"File container must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & IResource.PROJECT) == 0) {
			target.updateStatus(toString(),"File container must be a valid project.");
			return;
		}
		if (!container.isAccessible()) {
			target.updateStatus(toString(),"Project must be writable");
			return;
		}
		if (fileName.length() == 0) {
			target.updateStatus(toString(),"File name must be specified");
			return;
		}
		if (!TargetDefinitionRules.VALID_TARGET_NAME.matcher(fileName).matches()) {
			target.updateStatus(toString(),"Spaces and non-ASCII or non-word characters are forbidden in the target name, save for the . and _ separators.");
			return;
		}
		int dotLoc = fileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = fileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("properties") == true) {
				target.updateStatus(toString(),"The 'properties' extension is not part of the target name.");
				return;
			}
		}
		target.updateStatus(toString(),null);
	}

	public void modifyText(ModifyEvent e) {
		dialogChanged();
	}
}