/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.model;

import java.util.ArrayList;
import java.util.List;

public enum DSLHeadClause {
	
	DEFINE,LOAD,CONVERT,EXECUTE,ASSERT,VERIFY;
	
	public static DSLHeadClause get(String test) {
		try {
			return DSLHeadClause.valueOf(test);
		} catch (IllegalArgumentException e) {
			return null;
		}   
	}
	
	public static List<DSLHeadClause> startWith (String start){
		List<DSLHeadClause> list = new ArrayList<DSLHeadClause>();
		for (DSLHeadClause clause : DSLHeadClause.values()) {
			if(clause.name().startsWith(start.toUpperCase())){
				list.add(clause);
			}
		}
		return list;
	}

}
