/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.*;

/**
 * Scanner used to distinguish the different types of script lines during
 * syntactic coloring.
 * 
 * @author edegenetais
 * 
 */
public class DSLPartitionScanner extends RuleBasedPartitionScanner {
	
	public DSLPartitionScanner() {
		List<IPredicateRule> rules=new ArrayList<IPredicateRule>();
		for(DSLPartitionRuleSet type:DSLPartitionRuleSet.values()){
			rules.add(type.getRule());
		}
		setPredicateRules(rules.toArray(new IPredicateRule[rules.size()]));
	}
}
