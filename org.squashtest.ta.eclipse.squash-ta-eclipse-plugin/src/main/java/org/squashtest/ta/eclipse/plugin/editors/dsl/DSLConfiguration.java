/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.squashtest.ta.eclipse.plugin.editors.ColorManager;
import org.squashtest.ta.eclipse.plugin.editors.NonRuleBasedDamagerRepairer;
import org.squashtest.ta.eclipse.plugin.editors.completion.DSLContentAssistProcessor;

/**
 * This class is a factory responsible for the wiring of the Squash TA DSL editor elements.
 * @author edegenetais
 *
 */
public class DSLConfiguration extends SourceViewerConfiguration {
	private DSLDoubleClickStrategy doubleClickStrategy;
	private DSLElementScanner scanner;
	private ColorManager colorManager;

	public DSLConfiguration(ColorManager colorManager) {
		this.colorManager = colorManager;
	}
	
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		List<String> contentTypeIds=new ArrayList<String>();
		contentTypeIds.add(IDocument.DEFAULT_CONTENT_TYPE);
		contentTypeIds.addAll(DSLPartitionRuleSet.getContentTypeIdList());
		return contentTypeIds.toArray(new String[contentTypeIds.size()]);
	}
	
	public ITextDoubleClickStrategy getDoubleClickStrategy(
		ISourceViewer sourceViewer,
		String contentType) {
		if (doubleClickStrategy == null)
			doubleClickStrategy = new DSLDoubleClickStrategy();
		return doubleClickStrategy;
	}

	protected DSLElementScanner getElementScanner() {
		if (scanner == null) {
			scanner = new DSLElementScanner(colorManager);
			scanner.setDefaultReturnToken(new Token(new TextAttribute(colorManager.getColor(DSLColorMapping.DEFAULT))));
		}
		return scanner;
	}
	
	@Override
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
		ContentAssistant assistant=new ContentAssistant();
		/* 
		 * Note: actuellement l'autocomplétion n'est active que pour les éléments en noir du code (contenu par défaut)
		 * Si besoin de compléter des parties typées (eg macros), il faudra associer une instance de DSLContenProcessor
		 * avec le type de contenu défini pour les macros. 
		 */
		assistant.setContentAssistProcessor(new DSLContentAssistProcessor(),IDocument.DEFAULT_CONTENT_TYPE);
		assistant.setInformationControlCreator(new DSLInformationControlCreator());
		return assistant;
	}
	
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();

		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(getElementScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		for(DSLPartitionRuleSet rule:DSLPartitionRuleSet.values()){
			NonRuleBasedDamagerRepairer repairer =
				new NonRuleBasedDamagerRepairer(new TextAttribute(colorManager.getColor(rule.getAssociatedColor()),null,rule.getStyle()));
			reconciler.setDamager(repairer, rule.getId());
			reconciler.setRepairer(repairer, rule.getId());
		}
		
		return reconciler;
	}

}