/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.analyser;

/**
 * 
 *
 */
public class LineIdentifier {
	
	/** The line number in the TA script file */
	private int lineNumber;
	
	/** The line beginning offset in the TA script file */
	private int beginOffset;
	
	/** The length of the line */
	private int length;

	/**
	 * The full constructor
	 * 
	 * @param lineNumber The number of the line in the TA script file
	 * @param beginOffset The line beginning offset in the TA script file 
	 * @param length The line length
	 */
	public LineIdentifier(int lineNumber, int beginOffset, int length) {
		super();
		this.lineNumber = lineNumber;
		this.beginOffset = beginOffset;
		this.length = length;
	}

	/**
	 * Getter method 
	 * 
	 * @return The number of the line in the TA script file
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/**
	 * Getter method
	 * 
	 * @return The line beginning offset
	 */
	public int getBeginOffset() {
		return beginOffset;
	}

	/**
	 * Getter method
	 * 
	 * @return The line length
	 */
	public int getLength() {
		return length;
	}
	
	
}
