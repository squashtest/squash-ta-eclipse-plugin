/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.analyser;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.rules.ICharacterScanner;

/**
 * Adapter to use character scanner on arbitrary strings.
 * 
 * @author edegenetais
 * 
 */
public class AdapterCharacterScanner implements ICharacterScanner {
	private IDocument content;
	private int column;
	private int index;
	private char[][] legalDelimiters;

	/**
	 * Complete initialization constructor.
	 * 
	 * @param offset
	 *            the offset to use.
	 * @param sourceDoc
	 *            the source document handler.
	 * @throws BadLocationException
	 *             if the offset is not valid in the document.
	 */
	public AdapterCharacterScanner(int offset, ITextViewer sourceDoc)
			throws BadLocationException {
		String[] documentDelimiters = sourceDoc.getDocument().getLegalLineDelimiters();
		legalDelimiters = new char[documentDelimiters.length][];
		for (int delimiterIndex = 0; delimiterIndex < documentDelimiters.length; delimiterIndex++) {
			legalDelimiters[delimiterIndex] = documentDelimiters[delimiterIndex].toCharArray();
		}
		int currentLine = sourceDoc.getDocument().getLineOfOffset(offset);
		int lineBeginOffset = sourceDoc.getDocument().getLineOffset(currentLine);
		column = offset - lineBeginOffset;
		index = offset;
		content = sourceDoc.getDocument();
	}

	@Override
	public int getColumn() {
		return column;
	}

	@Override
	public char[][] getLegalLineDelimiters() {
		return legalDelimiters.clone();// yes, I want it read only
	}

	@Override
	public int read() {
		int charToRead;
		if (index < content.getLength()) {
			try {
				charToRead = content.getChar(index++);
			} catch (BadLocationException e) {
				charToRead = ICharacterScanner.EOF;
			}
			updateCurrentColumn();
		} else {
			charToRead = ICharacterScanner.EOF;
		}
		return charToRead;
	}

	@Override
	public void unread() {
		if (index > 0) {
			--index;
			updateCurrentColumn();
		}
	}

	private void updateCurrentColumn() {
		try {
			int currentLine = content.getLineOfOffset(index);
			column=index-content.getLineOffset(currentLine);
		} catch (BadLocationException e) {
			throw new IllegalStateException("Illegal offset "+index, e);
		}
	}

}
