/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl.rules.conditions

import org.eclipse.jface.text.rules.ICharacterScanner

public class TestCharacterScanner implements ICharacterScanner {
	private String[] content;
	private int column;
	private int line;
	
	public TestCharacterScanner(String content){
		this.content=content.split("\n")
	}
	
	@Override
	public int getColumn() {
		return column;
	}

	@Override
	public char[][] getLegalLineDelimiters() {
		return [['\n']]
	}

	@Override
	public int read() {
		if(column<content[line].length()){
			return content[line].charAt(column++);
		}else if(line<content.length-1){
			line++
			column=0
			return '\n';
		}else{
			return EOF;
		}
	}

	@Override
	public void unread() {
		if(column>0){
			column--
		}else if(line>0){
			line--;
			column=content[line].length();
		}
	}

}
