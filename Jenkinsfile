@Library('std-builds-library') _

pipeline {
    agent {
        kubernetes {
            inheritFrom 'maven-builder-jdk8'
            defaultContainer 'maven'
        }
    }

    environment {
            JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris -Duser.language=FR -Duser.country=FR -Duser.variant=FR"
            HOME = "${WORKSPACE}"
                    
    }
    parameters{
        choice(
            name : 'build_type',
            description : 'Choose between regular or release build',
            choices: ["regular", "release"]
        )

        string(
            name: "release_version",
            description: "the release version (if release)",
            defaultValue : ""
        )

        string(
            name: "next_version",
            description: "the next development version (if release)",
            defaultValue : ""
        )
    }    

    stages {
        
        stage('add hostname') {
            steps {
                
                sh "echo 137.226.34.46 ftp.halifax.rwth-aachen.de >> /etc/hosts"
               
            }   
            
        }         
        stage('Build') {
            when  {
                expression { params.build_type == 'regular' }
                  }         
            
            steps {           
                    
                sh 'mvn -B -ntp org.jacoco:jacoco-maven-plugin:prepare-agent deploy -Pci,public'

            }
            post {
                success {
                    script {
                            echo 'publishing installers'
                            withCredentials([usernamePassword(
                                        credentialsId: 'jenkins-nexus-credentials',
                                        passwordVariable: 'PASSWORD',
                                        usernameVariable: 'USER')]
                                        ) {
                                            //uploading installers
                                            sh '''
                                            cd org.squashtest.ta.eclipse.squash-ta-eclipse-repository/target/
                                            for installer in $(find . -name "org.squashtest.ta.eclipse.squash-ta-eclipse-repository-*.zip"); \
                                            do curl -i --fail --user "$USER:$PASSWORD" --upload-file "$installer" "https://nexus.squashtest.org/nexus/repository/binaries-acceptance/squash-tf/squash-tf-1/$installer"; done
                                            '''                                                      
                                        } 
                    }
                }
            }                       
        }
        stage('QA'){
            when  {
                expression { params.build_type == 'regular' }
                  }  
            steps {
                withSonarQubeEnv('SonarQube'){
                    sh 'mvn -Dsonar.scm.disabled=true sonar:sonar'
                }
                timeout(time: 10, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
            } 
        }
        // release : when the build is a release (serious game) build
        stage('release'){
            when{
                expression { params.build_type == 'release' }
            }
            steps {
                    container('maven') {
                        script {

                            def valid = release.isConfValid()
                            if (!valid) {
                                error('Cannot release : either the release version or next dev version is wrong')
                            }

                            def additional_release_profiles = ',public'
                            echo "releasing : "
                            release.release(additional_release_profiles)
                            echo "release complete !"

                        }
                    }

                }
            post{
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
                success {
                    script {
                        echo 'publishing installers'
                        publish.publish([
                            'public': true,
                            'archiveModule': './org.squashtest.ta.eclipse.squash-ta-eclipse-repository'
                        ])
                        release.addReleaseInfo(this)
                        release.push()
                    }
                }
            }
        }        
    }
}